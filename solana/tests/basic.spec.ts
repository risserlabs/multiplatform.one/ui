/**
 * File: /tests/basic.spec.ts
 * Project: solana
 * File Created: 13-01-2025 19:20:27
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as anchor from "@coral-xyz/anchor";
import type { Program } from "@coral-xyz/anchor";
import { PublicKey } from "@solana/web3.js";
import type { Basic } from "../target/types/basic";

describe("basic", () => {
  const provider = anchor.AnchorProvider.env();
  anchor.setProvider(provider);
  const program = anchor.workspace.Basic as Program<Basic>;
  it("should initialize the program", async () => {
    const [lockPda] = PublicKey.findProgramAddressSync(
      [Buffer.from("lock")],
      program.programId,
    );
    const vault = anchor.web3.Keypair.generate();
    const unlockTime = Math.floor(Date.now() / 1000) + 24 * 60 * 60;
    const depositAmount = new anchor.BN(1_000_000_000);
    await program.methods
      .initialize(new anchor.BN(unlockTime), depositAmount)
      .accounts({
        vault: vault.publicKey,
        authority: provider.wallet.publicKey,
      })
      .rpc();
    const lockAccount = await program.account.lock.fetch(lockPda);
    expect(lockAccount.unlockTime.toNumber()).toEqual(unlockTime);
    expect(lockAccount.owner.toBase58()).toEqual(
      provider.wallet.publicKey.toBase58(),
    );
    const vaultBalance = await provider.connection.getBalance(vault.publicKey);
    expect(vaultBalance).toEqual(depositAmount.toNumber());
  });

  it("should fail to withdraw before unlock time", async () => {
    const vault = anchor.web3.Keypair.generate();
    try {
      await program.methods
        .withdraw()
        .accounts({
          vault: vault.publicKey,
          owner: provider.wallet.publicKey,
        })
        .rpc();
    } catch (err) {
      expect((err as any).error.errorMessage).toEqual(
        "Cannot withdraw before unlock time",
      );
    }
  });
});
