/**
 * File: /test/Lock.ts
 * Project: ethereum
 * File Created: 12-01-2025 20:14:34
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  loadFixture,
  time,
} from "@nomicfoundation/hardhat-toolbox-viem/network-helpers";
import { expect } from "chai";
import hre from "hardhat";
import { getAddress, parseGwei } from "viem";

describe("Lock", () => {
  async function deployOneYearLockFixture() {
    const ONE_YEAR_IN_SECS = 365 * 24 * 60 * 60;
    const lockedAmount = parseGwei("1");
    const unlockTime = BigInt((await time.latest()) + ONE_YEAR_IN_SECS);
    const [owner, otherAccount] = await hre.viem.getWalletClients();
    const lock = await hre.viem.deployContract("Lock", [unlockTime], {
      value: lockedAmount,
    });
    const publicClient = await hre.viem.getPublicClient();
    return {
      lock,
      unlockTime,
      lockedAmount,
      owner,
      otherAccount,
      publicClient,
    };
  }

  describe("Deployment", () => {
    it("Should set the right unlockTime", async () => {
      const { lock, unlockTime } = await loadFixture(deployOneYearLockFixture);
      expect(await lock.read.unlockTime()).to.equal(unlockTime);
    });

    it("Should set the right owner", async () => {
      const { lock, owner } = await loadFixture(deployOneYearLockFixture);
      expect(await lock.read.owner()).to.equal(
        getAddress(owner.account.address),
      );
    });

    it("Should receive and store the funds to lock", async () => {
      const { lock, lockedAmount, publicClient } = await loadFixture(
        deployOneYearLockFixture,
      );
      expect(
        await publicClient.getBalance({
          address: lock.address,
        }),
      ).to.equal(lockedAmount);
    });

    it("Should fail if the unlockTime is not in the future", async () => {
      const latestTime = BigInt(await time.latest());
      await expect(
        hre.viem.deployContract("Lock", [latestTime], {
          value: 1n,
        }),
      ).to.be.rejectedWith("Unlock time should be in the future");
    });
  });

  describe("Withdrawals", () => {
    describe("Validations", () => {
      it("Should revert with the right error if called too soon", async () => {
        const { lock } = await loadFixture(deployOneYearLockFixture);
        await expect(lock.write.withdraw()).to.be.rejectedWith(
          "You can't withdraw yet",
        );
      });

      it("Should revert with the right error if called from another account", async () => {
        const { lock, unlockTime, otherAccount } = await loadFixture(
          deployOneYearLockFixture,
        );
        await time.increaseTo(unlockTime);
        const lockAsOtherAccount = await hre.viem.getContractAt(
          "Lock",
          lock.address,
          { client: { wallet: otherAccount } },
        );
        await expect(lockAsOtherAccount.write.withdraw()).to.be.rejectedWith(
          "You aren't the owner",
        );
      });

      it("Shouldn't fail if the unlockTime has arrived and the owner calls it", async () => {
        const { lock, unlockTime } = await loadFixture(
          deployOneYearLockFixture,
        );
        await time.increaseTo(unlockTime);
        await expect(lock.write.withdraw()).to.be.fulfilled;
      });
    });

    describe("Events", () => {
      it("Should emit an event on withdrawals", async () => {
        const { lock, unlockTime, lockedAmount, publicClient } =
          await loadFixture(deployOneYearLockFixture);
        await time.increaseTo(unlockTime);
        const hash = await lock.write.withdraw();
        await publicClient.waitForTransactionReceipt({ hash });
        const withdrawalEvents = await lock.getEvents.Withdrawal();
        expect(withdrawalEvents).to.have.lengthOf(1);
        expect((withdrawalEvents[0].args as any).amount).to.equal(lockedAmount);
      });
    });
  });
});
