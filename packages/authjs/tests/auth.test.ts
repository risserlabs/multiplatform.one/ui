/*
 * File: /tests/auth.test.ts
 * Project: @multiplatform.one/authjs
 * File Created: 17-01-2025 21:56:45
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { AuthConfig } from "@auth/core";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { authHandler, getAuthUrl, getAuthUser, verifyAuth } from "../src";

describe("authjs", () => {
  let mockConfig: AuthConfig;
  let mockRequest: Request;

  beforeEach(() => {
    mockConfig = {
      secret: "test-secret",
      basePath: "/auth",
      providers: [],
    };
    mockRequest = new Request("http://localhost:3000/auth/session", {
      headers: new Headers({
        cookie: "next-auth.session-token=xyz",
      }),
    });
  });

  describe("getAuthUrl", () => {
    it("should return correct auth URL with default settings", () => {
      const url = getAuthUrl(mockRequest, mockConfig);
      expect(url).toBe("http://localhost:3000/auth");
    });

    it("should handle x-forwarded headers", () => {
      const request = new Request("http://localhost:3000/auth/session", {
        headers: new Headers({
          "x-forwarded-host": "example.com",
          "x-forwarded-proto": "https",
        }),
      });
      const url = getAuthUrl(request, mockConfig);
      expect(url).toBe("https://example.com/auth");
    });
  });

  describe("authHandler", () => {
    it("should throw error if secret is missing", async () => {
      const config = { ...mockConfig, secret: "" };
      await expect(authHandler(mockRequest, config)).rejects.toThrow(
        "Missing AUTH_SECRET",
      );
    });

    it("should return empty response for _log endpoint", async () => {
      const request = new Request("http://localhost:3000/auth/_log");
      const response = await authHandler(request, mockConfig);
      expect(response.body).toBeNull();
    });
  });

  describe("verifyAuth", () => {
    it("should throw 401 if user is not authenticated", async () => {
      await expect(verifyAuth(mockRequest, mockConfig)).rejects.toThrow();
    });
  });
});
