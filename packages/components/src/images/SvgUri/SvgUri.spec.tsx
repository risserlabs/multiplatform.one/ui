/**
 * File: /src/images/SvgUri/SvgUri.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 01-02-2025 04:24:48
 * Author: FFX User
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render } from "@testing-library/react";
import { Paragraph } from "tamagui";
import { SvgUri } from "./index";

describe("SvgUri Component", () => {
  it("renders without crashing with a valid uri", () => {
    const { container } = render(
      <SvgUri uri="https://example.com/icon.svg" width={100} height={100} />,
    );
    expect(container).toBeTruthy();
  });
  it("accepts and applies custom dimensions", () => {
    const { getByTestId } = render(
      <SvgUri
        uri="https://example.com/icon.svg"
        width={200}
        height={150}
        data-testid="custom-svg"
      />,
    );
    const svgElement = getByTestId("svg-uri");
    expect(svgElement.style.width).toBe("200px");
    expect(svgElement.style.height).toBe("150px");
  });
  it("accepts and applies custom styles", () => {
    const { getByTestId } = render(
      <SvgUri
        uri="https://example.com/icon.svg"
        data-testid="styled-svg"
        style={{ opacity: 0.5 }}
      />,
    );
    const svgElement = getByTestId("svg-uri");
    expect(svgElement).toBeDefined();
    expect(svgElement.style.opacity).toBe("0.5");
  });
  it("renders children correctly", () => {
    const { getByTestId } = render(
      <SvgUri uri="https://example.com/icon.svg" width={100} height={100}>
        <Paragraph data-testid="child-element">Child Element</Paragraph>
      </SvgUri>,
    );
    expect(getByTestId("child-element")).toBeTruthy();
  });
  describe("dynamic behavior", () => {
    it("updates dimensions when props change", () => {
      const { getByTestId, rerender } = render(
        <SvgUri
          uri="https://example.com/icon.svg"
          width={100}
          height={100}
          data-testid="dynamic-svg"
        />,
      );
      const svgElement = getByTestId("svg-uri");
      expect(svgElement.style.width).toBe("100px");
      expect(svgElement.style.height).toBe("100px");
      rerender(
        <SvgUri
          uri="https://example.com/icon.svg"
          width={200}
          height={150}
          data-testid="dynamic-svg"
        />,
      );
      expect(svgElement.style.width).toBe("200px");
      expect(svgElement.style.height).toBe("150px");
    });
    it("updates styles dynamically", () => {
      const { getByTestId, rerender } = render(
        <SvgUri
          uri="https://example.com/icon.svg"
          width={100}
          height={100}
          style={{ opacity: 0.5 }}
          data-testid="dynamic-style-svg"
        />,
      );
      const svgElement = getByTestId("svg-uri");
      expect(svgElement.style.opacity).toBe("0.5");
      rerender(
        <SvgUri
          uri="https://example.com/icon.svg"
          width={100}
          height={100}
          style={{ opacity: 0.8 }}
          data-testid="dynamic-style-svg"
        />,
      );
      expect(svgElement.style.opacity).toBe("0.8");
    });
  });

  describe("edge cases", () => {
    it("handles empty uri", () => {
      const { container } = render(<SvgUri uri="" />);
      const svgElement = container.querySelector("svg");
      expect(svgElement).toBeNull();
    });
    it("handles invalid uri", () => {
      const { container } = render(<SvgUri uri="invalid-uri" />);
      const svgElement = container.querySelector("svg");
      expect(svgElement).toBeNull();
    });
  });
});
