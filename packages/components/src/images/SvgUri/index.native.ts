/*
 * File: /src/images/SvgUri/index.native.ts
 * Project: @multiplatform.one/components
 * File Created: 01-01-1970 00:00:00
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SvgUri as RNSvgUri } from "react-native-svg";
import { styled } from "tamagui";

export const SvgUri = styled(RNSvgUri, { uri: null });
