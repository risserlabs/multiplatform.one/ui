/**
 * File: /src/mdx/LI/LI.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 05-02-2025 11:27:59
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import { LI } from "./index";

describe("LI Component", () => {
  it("should render without errors", () => {
    const { getByText } = render(<LI>This is a list item</LI>);
    expect(getByText("This is a list item")).toBeDefined();
  });

  it("should have the correct tag", () => {
    const { container } = render(<LI>This is a list item</LI>);
    const listItem = container.querySelector("li");
    expect(listItem).not.toBeNull();
  });

  it("should render dynamic content correctly", () => {
    const { getByText } = render(<LI>Another list item</LI>);
    expect(getByText("Another list item")).toBeDefined();
  });

  it("should have display: list-item", () => {
    const { container } = render(<LI>This is a list item</LI>);
    const listItem = container.querySelector("li");
    expect(listItem).toHaveStyle("display: list-item");
  });
});
