import { render, screen } from "@testing-library/react";
import type { MDXComponents } from "mdx/types";
/**
 * File: /src/mdx/MDX/MDX.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 05-02-2025 04:31:00
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { YStack } from "tamagui";
import { vi } from "vitest";
import { useCompileAndEvaluateMdx } from "../hooks";
import { MDX } from "./index";

const DummyComponent = ({
  components,
}: {
  components: MDXComponents;
}) => {
  return <YStack data-testid="dummy">{JSON.stringify(components)}</YStack>;
};
vi.mock("../hooks", () => ({
  useCompileAndEvaluateMdx: vi.fn(),
}));
vi.mock("@mdx-js/react", () => ({
  useMDXComponents: () => ({ hookKey: "hookValue" }),
}));
vi.mock("../components", () => ({
  mdxComponents: { mdxKey: "mdxValue" },
}));

describe("MDX component", () => {
  afterEach(() => {
    vi.clearAllMocks();
  });
  it("renders an empty fragment if no component is returned from the hook", () => {
    (useCompileAndEvaluateMdx as any).mockReturnValue({
      Component: null,
      code: "some code",
      frontmatter: { test: "frontmatter" },
    });
    const { container } = render(<MDX {...{ children: "hello" }} />);
    expect(container.innerHTML).toBe("");
  });

  it("renders the evaluated MDX component and passes the merged components prop", () => {
    (useCompileAndEvaluateMdx as any).mockReturnValue({
      Component: DummyComponent,
      code: "compiledCode",
      frontmatter: { test: true },
    });
    const additionalComponents = { propKey: "propValue" };
    render(
      //@ts-ignore
      <MDX {...{ children: "hello" }} components={additionalComponents} />,
    );
    const dummyDiv = screen.getByTestId("dummy");
    const renderedComponents = JSON.parse(dummyDiv.textContent || "");
    expect(renderedComponents).toEqual({
      mdxKey: "mdxValue",
      hookKey: "hookValue",
      propKey: "propValue",
    });
  });

  it("calls debug logging when the debug prop is provided", () => {
    const debugSpy = vi.spyOn(console, "debug").mockImplementation(() => {});
    (useCompileAndEvaluateMdx as any).mockReturnValue({
      Component: DummyComponent,
      code: "compiledCode",
      frontmatter: { test: "frontmatter" },
    });
    render(<MDX {...{ children: "hello" }} debug={true} />);
    expect(debugSpy).toHaveBeenCalledWith("MDX", {
      frontmatter: { test: "frontmatter" },
    });
    expect(debugSpy).toHaveBeenCalledWith("compiledCode");
    debugSpy.mockRestore();
  });

  it("does not call debug logging when the debug prop is false", () => {
    const debugSpy = vi.spyOn(console, "debug").mockImplementation(() => {});
    (useCompileAndEvaluateMdx as any).mockReturnValue({
      Component: DummyComponent,
      code: "compiledCode",
      frontmatter: { test: "frontmatter" },
    });
    render(<MDX {...{ children: "hello" }} debug={false} />);
    expect(debugSpy).not.toHaveBeenCalled();
    debugSpy.mockRestore();
  });

  it("does not throw an error when an empty children string is provided", () => {
    (useCompileAndEvaluateMdx as any).mockReturnValue({
      Component: DummyComponent,
      code: "",
      frontmatter: {},
    });
    expect(() => render(<MDX {...{ children: "hello" }} />)).not.toThrow();
  });
});
