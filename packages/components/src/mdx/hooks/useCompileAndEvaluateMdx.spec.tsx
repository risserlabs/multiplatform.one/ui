/**
 * File: /src/mdx/hooks/useCompileAndEvaluateMdx.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 05-02-2025 10:07:45
 * Author: abhiramkaleru
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render, renderHook, screen, waitFor } from "@testing-library/react";
import { describe, expect, it, vi } from "vitest";
import { useCompileAndEvaluateMdx } from "./useCompileAndEvaluateMdx";
import { useCompileMdx } from "./useCompileMdx";
import { useEvaluateMdx } from "./useEvaluateMdx";

describe("useCompileMdx Hook", () => {
  it("should handle invalid MDX syntax gracefully", async () => {
    const invalidSource = "# Test\n\nSome broken MDX content";
    const { result } = renderHook(() => useCompileMdx(invalidSource));
    await waitFor(() => expect(result.current.code).toBeUndefined());
    expect(result.current.frontmatter).toBeUndefined();
  });
});

describe("useCompileAndEvaluateMdx Hook", () => {
  it("should handle empty source gracefully", async () => {
    const source = "";
    const { result } = renderHook(() => useCompileAndEvaluateMdx(source));
    await waitFor(() => expect(result.current.code).toBeUndefined());
    expect(result.current.Component).toBeUndefined();
    expect(result.current.frontmatter).toBeUndefined();
  });

  it("should return empty if MDX compilation or evaluation fails", async () => {
    const source = "# Invalid MDX Content";
    const { result } = renderHook(() => useCompileAndEvaluateMdx(source));
    await waitFor(() => expect(result.current.code).toBeUndefined());
    expect(result.current.Component).toBeUndefined();
    expect(result.current.frontmatter).toBeUndefined();
  });
});
