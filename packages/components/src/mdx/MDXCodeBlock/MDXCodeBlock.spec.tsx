/**
 * File: /src/mdx/MDXCodeBlock/MDXCodeBlock.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 11-02-2025 07:19:41
 * Author: ganeshaerpula
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { useContext } from "react";
import React from "react";
import { Text } from "tamagui";
import { Stack } from "tamagui";
import { vi } from "vitest";
import * as MDXCodeBlockContext from "./MDXCodeBlockContext";
import { MDXCodeBlock } from "./index";

const TestComponent = () => {
  const context = useContext(MDXCodeBlockContext.MDXCodeBlockContext);
  return (
    <Stack>
      <Text>{`Disable Copy: ${context.disableCopy ? "true" : "false"}`}</Text>
      <Text>{`Is Collapsible: ${context.isCollapsible ? "true" : "false"}`}</Text>
      <Text>{`Show Line Numbers: ${context.showLineNumbers ? "true" : "false"}`}</Text>
    </Stack>
  );
};

vi.mock("../../hooks/useClipboard", () => ({
  useClipboard: () => ({
    hasCopied: false,
    onCopy: vi.fn(),
  }),
}));

class ErrorBoundary extends React.Component<{ children: React.ReactNode }> {
  state = { hasError: false };
  static getDerivedStateFromError(error: Error) {
    return { hasError: true };
  }
  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    console.error("Error caught in ErrorBoundary:", error, errorInfo);
  }
  render() {
    if (this.state.hasError) {
      return <Text>Error occurred while rendering the code block.</Text>;
    }
    return this.props.children;
  }
}

describe("MDXCodeBlock", () => {
  it("should render the code block correctly", () => {
    render(
      <ErrorBoundary>
        <MDXCodeBlock>
          {"const x = 10;\nconst y = 20;\nconsole.log(x + y);"}
        </MDXCodeBlock>
      </ErrorBoundary>,
    );
    expect(screen.getByText(/const x = 10;/)).toBeInTheDocument();
    expect(screen.getByText(/const y = 20;/)).toBeInTheDocument();
    expect(screen.getByText(/console\.log\(x \+ y\);/)).toBeInTheDocument();
  });

  it("should render with custom language syntax highlighting", () => {
    render(<MDXCodeBlock>{"const x = 10;"}</MDXCodeBlock>);
    expect(screen.getByText(/const x = 10;/)).toBeInTheDocument();
  });

  it("should not render code block when no code is provided", () => {
    render(<MDXCodeBlock>{""}</MDXCodeBlock>);
    expect(screen.queryByText(/const x = 10;/)).toBeNull();
  });

  it("should toggle collapsible code block correctly", async () => {
    render(
      <MDXCodeBlock isCollapsible={true}>
        {"const x = 10;\nconst y = 20;\nconsole.log(x + y);"}
      </MDXCodeBlock>,
    );
    expect(
      screen.getByRole("button", { name: /show or hide code/i }),
    ).toBeInTheDocument();
    fireEvent.click(screen.getByRole("button", { name: /show or hide code/i }));
    await waitFor(() =>
      expect(
        screen.getByRole("button", { name: /hide code/i }),
      ).toBeInTheDocument(),
    );
    expect(
      screen.getByText(
        (content, element) =>
          element?.tagName.toLowerCase() === "code" &&
          content.includes("const x = 10;"),
      ),
    ).toBeInTheDocument();
    fireEvent.click(screen.getByRole("button", { name: /hide code/i }));
    await waitFor(() => expect(screen.queryByText(/const x = 10;/)).toBeNull());
  });

  it("should render with line numbers when showLineNumbers is true", () => {
    render(
      <MDXCodeBlock showLineNumbers={true}>
        {"const x = 10;\nconst y = 20;\nconsole.log(x + y);"}
      </MDXCodeBlock>,
    );
    expect(screen.getByText(/1/)).toBeInTheDocument();
    expect(screen.getByText(/2/)).toBeInTheDocument();
  });

  it("should not show the copy button when disableCopy is true", () => {
    render(
      <MDXCodeBlock disableCopy={true}>
        {"const x = 10;\nconst y = 20;\nconsole.log(x + y);"}
      </MDXCodeBlock>,
    );
    expect(screen.queryByLabelText(/Copy code to clipboard/)).toBeNull();
  });

  it("should render with custom className", () => {
    render(
      <MDXCodeBlock className="custom-class">{"const x = 10;"}</MDXCodeBlock>,
    );
    const codeBlock = screen.getByText(/const x = 10;/).closest("pre");
    expect(codeBlock).toHaveClass("custom-class");
  });

  it("should render with a specific id", () => {
    render(<MDXCodeBlock id="unique-id">{"const x = 10;"}</MDXCodeBlock>);
    const codeBlock = screen.getByText(/const x = 10;/).closest("pre");
    expect(codeBlock).toHaveAttribute("id", "unique-id");
  });

  it("should render with a specific className", () => {
    render(
      <MDXCodeBlock className="test-class">{"const x = 10;"}</MDXCodeBlock>,
    );
    const codeBlock = screen.getByText(/const x = 10;/).closest("pre");
    expect(codeBlock).toHaveClass("test-class");
  });

  it("should toggle collapsible code block correctly when long", async () => {
    const longCode = Array(30).fill("const x = 10;").join("\n");
    render(<MDXCodeBlock isCollapsible={true}>{longCode}</MDXCodeBlock>);
    expect(
      screen.getByRole("button", { name: /show or hide code/i }),
    ).toBeInTheDocument();
    fireEvent.click(screen.getByRole("button", { name: /show or hide code/i }));
    await waitFor(() =>
      expect(
        screen.getByRole("button", { name: /hide code/i }),
      ).toBeInTheDocument(),
    );
    expect(
      screen.getByText(
        (content, element) =>
          element?.tagName.toLowerCase() === "code" &&
          content.includes("const x = 10;"),
      ),
    ).toBeInTheDocument();
    fireEvent.click(screen.getByRole("button", { name: /hide code/i }));
    await waitFor(() => expect(screen.queryByText(/const x = 10;/)).toBeNull());
  });

  it("should provide default context values", () => {
    render(
      <MDXCodeBlockContext.MDXCodeBlockContext.Provider value={{}}>
        <TestComponent />
      </MDXCodeBlockContext.MDXCodeBlockContext.Provider>,
    );
    expect(screen.getByText("Disable Copy: false")).toBeDefined();
    expect(screen.getByText("Is Collapsible: false")).toBeDefined();
    expect(screen.getByText("Show Line Numbers: false")).toBeDefined();
  });

  it("should update context values", () => {
    const contextValue: MDXCodeBlockContext.MDXCodeBlockContextValue = {
      disableCopy: true,
      isCollapsible: true,
      isHighlightingLines: false,
      showLineNumbers: true,
    };
    render(
      <MDXCodeBlockContext.MDXCodeBlockContext.Provider value={contextValue}>
        <TestComponent />
      </MDXCodeBlockContext.MDXCodeBlockContext.Provider>,
    );
    expect(screen.getByText("Disable Copy: true")).toBeDefined();
    expect(screen.getByText("Is Collapsible: true")).toBeDefined();
    expect(screen.getByText("Show Line Numbers: true")).toBeDefined();
  });

  it("should handle long code correctly", () => {
    const longCode = Array(100).fill("const x = 10;").join("\n");
    render(<MDXCodeBlock>{longCode}</MDXCodeBlock>);
    expect(screen.getByText(/const x = 10;/)).toBeInTheDocument();
    expect(screen.getByText(/const x = 10;/)).toBeInTheDocument();
    expect(screen.getByText(/const x = 10;/).closest("code")).toHaveTextContent(
      "const x = 10;",
    );
  });
});
