/**
 * File: /src/forms/TextArea/FieldTextArea.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 30-01-2025 11:32:32
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import type { FieldValidateOrFn } from "@tanstack/form-core";
import type { FieldApi } from "@tanstack/form-core";
import type { FormApi } from "@tanstack/react-form";
import { useForm } from "@tanstack/react-form";
import { act, fireEvent, render, screen } from "@testing-library/react";
import { TamaguiProvider, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { Button } from "../Button";
import { Form } from "../Form";
import { FieldTextArea } from "./FieldTextArea";

const tamaguiConfig = createTamagui(config);

describe("FieldTextArea", () => {
  const createMockForm = () =>
    ({
      handleSubmit: vi.fn(),
      options: {},
      store: {},
      state: {},
      fieldInfo: {},
      getFieldValue: vi.fn(),
      setFieldValue: vi.fn(),
      getMeta: vi.fn(),
      setMeta: vi.fn(),
      validate: vi.fn(),
      reset: vi.fn(),
      isMutating: false,
      isValidating: false,
      isSubmitting: false,
      submitCount: 0,
      defaultValues: {},
      formState: {},
      fieldElements: new Map(),
    }) as unknown as FormApi<unknown>;

  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  describe("without form context", () => {
    it("should render successfully", () => {
      const mockForm = createMockForm();
      const { getByPlaceholderText } = render(
        <FieldTextArea
          form={mockForm}
          textAreaProps={{ placeholder: "Enter text" }}
        />,
      );
      expect(getByPlaceholderText("Enter text")).toBeDefined();
    });

    it("should handle text changes", () => {
      const onChangeText = vi.fn();
      const { getByPlaceholderText } = render(
        <FieldTextArea
          name="textArea"
          onChangeText={onChangeText}
          textAreaProps={{ placeholder: "Enter text" }}
        />,
      );
      const textarea = getByPlaceholderText("Enter text");
      fireEvent.change(textarea, { target: { value: "Test input" } });
      expect(onChangeText).toHaveBeenCalledWith("Test input");
    });
  });

  describe("with form context", () => {
    const FormExample = ({
      defaultValue = "",
      name = "textArea",
      onSubmit = async (_value: any) => {},
    }) => {
      const form = useForm({
        defaultValues: {
          [name]: defaultValue,
        },
        onSubmit: async ({ value }) => {
          await onSubmit(value);
        },
      });
      return (
        <Form form={form}>
          <FieldTextArea
            form={form}
            name={name}
            textAreaProps={{ placeholder: "Enter text" }}
          />
          <Button onPress={form.handleSubmit}>Submit</Button>
        </Form>
      );
    };

    it("should handle form submission", async () => {
      const onSubmit = vi.fn();
      const { getByPlaceholderText, getByText } = renderWithProviders(
        <FormExample onSubmit={onSubmit} />,
      );
      const textarea = getByPlaceholderText("Enter text");
      const submitButton = getByText("Submit");
      await act(async () => {
        fireEvent.change(textarea, { target: { value: "Test input" } });
      });
      await act(async () => {
        fireEvent.click(submitButton);
      });
      expect(onSubmit).toHaveBeenCalledWith({ textArea: "Test input" });
    });
  });

  describe("error handling and blur events", () => {
    const FormWithValidation = ({
      onSubmit = async (_value: any) => {},
      onBlur = () => {},
    }) => {
      const form = useForm({
        defaultValues: {
          textArea: "",
        },
        onSubmit: async ({ value }) => {
          await onSubmit(value);
        },
      });
      const validators = [
        {
          validator: ({ value }: { value: string }) => {
            if (!value) return "Field is required";
            return undefined;
          },
        },
        {
          validator: ({ value }: { value: string }) => {
            if (value && !/^[A-Za-z\s]*$/.test(value)) {
              return "Only letters allowed";
            }
            return undefined;
          },
        },
      ];

      return (
        <Form form={form}>
          <FieldTextArea
            form={form}
            name="textArea"
            onBlur={onBlur}
            textAreaProps={{ placeholder: "Enter text" }}
          />
        </Form>
      );
    };

    it("should handle custom error prop", () => {
      const { getByText } = renderWithProviders(
        <FieldTextArea
          error="Custom error message"
          textAreaProps={{ placeholder: "Enter text" }}
        />,
      );
      expect(getByText("Custom error message")).toBeInTheDocument();
    });

    it("should call both form and custom onBlur handlers", async () => {
      const customOnBlur = vi.fn();
      const { getByPlaceholderText } = renderWithProviders(
        <FormWithValidation onBlur={customOnBlur} />,
      );
      const textarea = getByPlaceholderText("Enter text");
      await act(async () => {
        fireEvent.blur(textarea);
      });
      expect(customOnBlur).toHaveBeenCalled();
    });
  });
});
