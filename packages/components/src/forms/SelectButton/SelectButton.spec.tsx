/**
 * File: /src/forms/SelectButton/SelectButton.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 31-01-2025 04:23:39
 * Author: FFX User
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { act, fireEvent, render } from "@testing-library/react";
import { TamaguiProvider, Text, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SelectButton } from "./SelectButton";

const tamaguiConfig = createTamagui(config);

describe("SelectButton", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  describe("selection behavior", () => {
    it("should handle option selection", async () => {
      const onValueChange = vi.fn();
      const { getAllByRole } = renderWithProviders(
        <SelectButton onValueChange={onValueChange}>
          <SelectButton.OptionButton index={0} value="option1">
            <Text>Option 1</Text>
          </SelectButton.OptionButton>
          <SelectButton.OptionButton index={1} value="option2">
            <Text>Option 2</Text>
          </SelectButton.OptionButton>
        </SelectButton>,
      );
      const buttons = getAllByRole("button");
      await act(async () => {
        fireEvent.click(buttons[0]);
      });
      expect(onValueChange).toHaveBeenCalledWith("option1");
    });

    it("should update selection when selectedValue prop changes", () => {
      const { getAllByRole, rerender } = renderWithProviders(
        <SelectButton selectedValue="option1">
          <SelectButton.OptionButton index={0} value="option1">
            <Text>Option 1</Text>
          </SelectButton.OptionButton>
          <SelectButton.OptionButton index={1} value="option2">
            <Text>Option 2</Text>
          </SelectButton.OptionButton>
        </SelectButton>,
      );
      const buttons = getAllByRole("button");
      expect(buttons[0]).toHaveStyle({
        backgroundColor: "var(--color7)",
      });
      rerender(
        <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
          <SelectButton selectedValue="option2">
            <SelectButton.OptionButton index={0} value="option1">
              <Text>Option 1</Text>
            </SelectButton.OptionButton>
            <SelectButton.OptionButton index={1} value="option2">
              <Text>Option 2</Text>
            </SelectButton.OptionButton>
          </SelectButton>
        </TamaguiProvider>,
      );
      expect(buttons[1]).toHaveStyle({
        backgroundColor: "var(--color7)",
      });
    });
  });

  describe("custom styling", () => {
    it("should apply custom selectedStyle to selected option", async () => {
      const customStyle = {
        backgroundColor: "$blue8",
      };
      const { getAllByRole } = renderWithProviders(
        <SelectButton selectedStyle={customStyle}>
          <SelectButton.OptionButton index={0} value="option1">
            <Text>Option 1</Text>
          </SelectButton.OptionButton>
          <SelectButton.OptionButton index={1} value="option2">
            <Text>Option 2</Text>
          </SelectButton.OptionButton>
        </SelectButton>,
      );
      const buttons = getAllByRole("button");
      await act(async () => {
        fireEvent.click(buttons[0]);
      });
      expect(buttons[0]).toHaveStyle({
        backgroundColor: "var(--blue8)",
      });
    });
  });

  describe("real-world scenarios", () => {
    it("should work as a survey/quiz answer selector", async () => {
      const onValueChange = vi.fn();
      const { getAllByRole, getByText } = renderWithProviders(
        <SelectButton onValueChange={onValueChange}>
          <SelectButton.OptionButton index={0} value="red">
            <Text>Red</Text>
          </SelectButton.OptionButton>
          <SelectButton.OptionButton index={1} value="blue">
            <Text>Blue</Text>
          </SelectButton.OptionButton>
          <SelectButton.OptionButton index={2} value="green">
            <Text>Green</Text>
          </SelectButton.OptionButton>
        </SelectButton>,
      );
      expect(getByText("Red")).toBeDefined();
      expect(getByText("Blue")).toBeDefined();
      expect(getByText("Green")).toBeDefined();
      const buttons = getAllByRole("button");
      await act(async () => {
        fireEvent.click(buttons[1]);
      });
      expect(onValueChange).toHaveBeenCalledWith("blue");
    });

    it("should handle product configuration selection", async () => {
      const onValueChange = vi.fn();
      const { getAllByRole } = renderWithProviders(
        <SelectButton onValueChange={onValueChange} xStack>
          <SelectButton.OptionButton index={0} value="small">
            <Text>Small</Text>
          </SelectButton.OptionButton>
          <SelectButton.OptionButton index={1} value="medium">
            <Text>Medium</Text>
          </SelectButton.OptionButton>
          <SelectButton.OptionButton index={2} value="large">
            <Text>Large</Text>
          </SelectButton.OptionButton>
        </SelectButton>,
      );
      const buttons = getAllByRole("button");
      await act(async () => {
        fireEvent.click(buttons[1]);
      });
      expect(onValueChange).toHaveBeenCalledWith("medium");
    });
  });
});
