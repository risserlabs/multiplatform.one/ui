/**
 * File: /src/forms/forms.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 11-02-2025 10:31:42
 * Author: shiva chennoji 7
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render, screen } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import { vi } from "vitest";
vi.mock("@tanstack/react-form", () => ({
  useForm: () => ({
    name: "defaultForm",
    one: {},
  }),
}));
import { Text } from "tamagui";
import { Form, useFormContext } from "./Form";
import { FormField } from "./FormField";
import * as FormsIndex from "./index";
import * as FormTypes from "./types";

function DummyFormConsumer(): JSX.Element {
  const form = useFormContext<any, any>();
  return <Text data-testid="dummy-form">{form ? form : "no-form"}</Text>;
}

describe("Form Component", () => {
  it("renders children", () => {
    render(
      <Form form={{ name: "dummyForm", one: {} } as any}>
        <Text data-testid="child">Child Content</Text>
      </Form>,
    );
    expect(screen.getByTestId("child").textContent).toBe("Child Content");
  });

  it("renders children when no form prop is provided", () => {
    render(
      <Form>
        <Text data-testid="child-no-form">Child Without Form Prop</Text>
      </Form>,
    );
    expect(screen.getByTestId("child-no-form").textContent).toBe(
      "Child Without Form Prop",
    );
  });
});

describe("Index Exports", () => {
  it("should export the Form component", () => {
    expect(FormsIndex.Form).toBeDefined();
  });

  it("should export the FormField component", () => {
    expect(FormsIndex.FormField).toBeDefined();
  });
});

describe("Types Module", () => {
  it("should be defined", () => {
    expect(FormTypes).toBeDefined();
  });
});

describe("FormField Component", () => {
  it("renders the label with a required asterisk when required is true", () => {
    render(
      <FormField id="test-label" label="Test Label" required>
        <Text data-testid="input">Input Content</Text>
      </FormField>,
    );
    const labelElement = screen.getByText((content, node) => {
      return node?.textContent === "Test Label *";
    });
    expect(labelElement).toBeDefined();
  });

  it("renders helper text from field error if provided", () => {
    const dummyField = {
      state: { meta: { errors: ["Error message"] } },
    } as any;
    render(
      <FormField label="Field Label" field={dummyField}>
        <Text data-testid="input">Input Content</Text>
      </FormField>,
    );
    const helperText = screen.getByText("Error message");
    expect(helperText).toBeDefined();
  });

  it("renders children properly", () => {
    render(
      <FormField label="Child Test">
        <Text data-testid="child-content">Child Content</Text>
      </FormField>,
    );
    expect(screen.getByTestId("child-content").textContent).toBe(
      "Child Content",
    );
  });
});
