/**
 * File: /src/icons/icons.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 12-02-2025 10:57:55
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import { MinusRegular } from "./MinusRegular";

describe("MinusRegular", () => {
  it("should render correctly", () => {
    const { container } = render(<MinusRegular />);
    expect(container.querySelector("svg")).toBeInTheDocument();
  });

  it("should have the correct default size", () => {
    const { container } = render(<MinusRegular />);
    const svg = container.querySelector("svg");
    expect(svg).toHaveAttribute("height", "24");
    expect(svg).toHaveAttribute("width", "24");
  });

  it("should accept custom color and size", () => {
    const { container } = render(<MinusRegular color="red" size={48} />);
    const line = container.querySelector("line");
    const svg = container.querySelector("svg");
    expect(line).toHaveAttribute("stroke", "red");
    expect(svg).toHaveAttribute("height", "48");
    expect(svg).toHaveAttribute("width", "48");
  });
});
