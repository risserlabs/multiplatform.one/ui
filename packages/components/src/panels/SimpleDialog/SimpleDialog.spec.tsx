/**
 * File: /src/panels/SimpleDialog/SimpleDialog.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 05-02-2025 05:57:25
 * Author: Vandana Madhireddy
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { Button, Text } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SimpleDialog } from "./index";

describe("SimpleDialog", () => {
  it("renders trigger element when provided", () => {
    render(
      <SimpleDialog trigger={<Button>Open Dialog</Button>}>
        Content
      </SimpleDialog>,
    );
    expect(screen.getByText("Open Dialog")).toBeInTheDocument();
  });

  it("renders custom trigger", () => {
    render(
      <SimpleDialog trigger={<Button>Custom Trigger</Button>}>
        Content
      </SimpleDialog>,
    );
    expect(screen.getByText("Custom Trigger")).toBeInTheDocument();
  });

  describe("SimpleDialog", () => {
    it("calls onOpenChange when dialog state changes", () => {
      const onOpenChange = vi.fn();
      render(
        <SimpleDialog
          trigger={<Button>Open Dialog</Button>}
          open={true}
          onOpenChange={onOpenChange}
        >
          Content
        </SimpleDialog>,
      );
      expect(screen.getByText("Open Dialog")).toBeInTheDocument();
      fireEvent.click(screen.getByText("Open Dialog"));
      expect(onOpenChange).toHaveBeenCalledWith(false);
    });
  });

  describe("Content Rendering", () => {
    it("renders title and description", () => {
      render(
        <SimpleDialog
          open
          title="Dialog Title"
          description="Dialog Description"
        >
          Content
        </SimpleDialog>,
      );
      expect(screen.getByText("Dialog Title")).toBeInTheDocument();
      expect(screen.getByText("Dialog Description")).toBeInTheDocument();
    });

    it("renders custom content", () => {
      render(
        <SimpleDialog
          open
          title="Dialog Title"
          description="Dialog Description"
        >
          <Text>Custom Content</Text>
        </SimpleDialog>,
      );
      expect(screen.getByText("Custom Content")).toBeInTheDocument();
    });
  });

  describe("Interactions", () => {
    it("does not render close button when withoutCloseButton is true", () => {
      render(
        <SimpleDialog open withoutCloseButton>
          Content
        </SimpleDialog>,
      );
      expect(screen.queryByRole("button")).not.toBeInTheDocument();
    });
  });

  describe("Edge Cases", () => {
    it("should handle missing title", () => {
      render(<SimpleDialog open>Content</SimpleDialog>);
      expect(screen.queryByTestId("dialog-title")).toBeNull();
    });

    it("should handle missing description", () => {
      render(
        <SimpleDialog open title="Dialog Title">
          Content
        </SimpleDialog>,
      );
      expect(screen.queryByTestId("dialog-description")).toBeNull();
    });
  });

  describe("SimpleDialog Debug", () => {
    it("renders dialog with asLeftSideSheet", () => {
      render(
        <SimpleDialog open asLeftSideSheet transitionWidth={150}>
          Content
        </SimpleDialog>,
      );
      console.log(document.body.innerHTML);
      expect(screen.getByText("Content")).toBeInTheDocument();
    });

    it("renders dialog with asRightSideSheet", () => {
      render(
        <SimpleDialog open asRightSideSheet transitionWidth={150}>
          Content
        </SimpleDialog>,
      );
      console.log(document.body.innerHTML);
      expect(screen.getByText("Content")).toBeInTheDocument();
    });
  });

  it("logs props.open value", () => {
    const { rerender } = render(
      <SimpleDialog open={false}>Content</SimpleDialog>,
    );
    console.log("Initial props.open:", false);
    rerender(<SimpleDialog open={true}>Content</SimpleDialog>);
    console.log("Updated props.open:", true);
  });

  it("prevents closing when withoutCloseButton is true", () => {
    const onOpenChange = vi.fn();
    render(
      <SimpleDialog open withoutCloseButton onOpenChange={onOpenChange}>
        Content
      </SimpleDialog>,
    );
    fireEvent.click(document.body);
    expect(onOpenChange).not.toHaveBeenCalled();
  });
});
