/**
 * File: /src/organize/SimpleListItem/SimpleListItem.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 05-02-2025 16:58:20
 * Author: FFX
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { render, screen } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import { SimpleList, SimpleListItem } from "./index";

describe("SimpleListItem Component", () => {
  it("should render successfully within SimpleList", () => {
    render(
      <SimpleList orientation="vertical">
        <SimpleListItem title="Item 1" />
      </SimpleList>,
    );
    expect(screen.getByText("Item 1")).toBeInTheDocument();
  });

  it("should throw an error if used outside of SimpleList", () => {
    expect(() => render(<SimpleListItem title="Item 1" />)).toThrowError(
      "SimpleListItem must be used within a SimpleList",
    );
  });

  it("should render with correct background color from context", () => {
    render(
      <SimpleList backgroundColor="lightblue">
        <SimpleListItem title="Item 1" />
      </SimpleList>,
    );
    const item = screen.getByText("Item 1");
    expect(item).toHaveStyle({ backgroundColor: "default" });
  });

  it("should render with default background color if none provided", () => {
    render(
      <SimpleList>
        <SimpleListItem title="Item 1" />
      </SimpleList>,
    );
    const item = screen.getByText("Item 1");
    expect(item).toHaveStyle({ backgroundColor: "default" });
  });

  it("should render the correct title", () => {
    render(
      <SimpleList orientation="horizontal">
        <SimpleListItem title="Test Title" />
      </SimpleList>,
    );
    const item = screen.getByText("Test Title");
    expect(item).toBeInTheDocument();
  });
});
