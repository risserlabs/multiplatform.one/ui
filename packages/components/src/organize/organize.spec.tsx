/**
 * File: /src/organize/organize.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 01-01-1970 00:00:00
 * Author: Tarun2811

 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { vi } from "vitest";
import * as OrganizeComponents from "./index";

vi.mock("one", () => ({
  createApp: vi.fn(),
  usePathname: vi.fn(),
}));

describe("Component Exports", () => {
  it("should export SimpleAccordion", () => {
    expect(OrganizeComponents.SimpleAccordion).toBeDefined();
  });

  it("should export SimpleCarousel", () => {
    expect(OrganizeComponents.SimpleCarousel).toBeDefined();
  });
  it("should export SimpleInput", () => {
    expect(OrganizeComponents.SimpleInput).toBeDefined();
  });

  it("should export SimpleListItem", () => {
    expect(OrganizeComponents.SimpleListItem).toBeDefined();
  });

  it("should export SimpleTabs", () => {
    expect(OrganizeComponents.SimpleTabs).toBeDefined();
  });

  it("should export SimpleParagraph", () => {
    expect(OrganizeComponents.SimpleParagraph).toBeDefined();
  });
});
