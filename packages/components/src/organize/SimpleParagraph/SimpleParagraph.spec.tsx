/**
 * File: /src/organize/SimpleParagraph/SimpleParagraph.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 11-02-2025 07:08:49
 * Author: Vandana Madhireddy
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { render } from "@testing-library/react";
import { Paragraph, TamaguiProvider, YStack, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SimpleParagraph } from "./index";

vi.mock("../../code", () => ({
  CodeBlock: ({ children }) => (
    <YStack data-testid="code-block">
      <Paragraph>{children}</Paragraph>
    </YStack>
  ),
  Pre: ({ children }) => (
    <YStack data-testid="pre">
      <Paragraph>{children}</Paragraph>
    </YStack>
  ),
}));

const tamaguiConfig = createTamagui(config);

describe("SimpleParagraph", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  describe("Text Rendering", () => {
    it("should render plain text correctly", () => {
      const { getByText } = renderWithProviders(
        <SimpleParagraph>Simple text content</SimpleParagraph>,
      );
      expect(getByText("Simple text content")).toBeDefined();
    });

    it("should render bold text correctly", () => {
      const { getByText } = renderWithProviders(
        <SimpleParagraph>This is **bold** text</SimpleParagraph>,
      );
      expect(getByText("bold")).toHaveStyle({ fontWeight: "bold" });
    });

    it("should handle multiple bold sections", () => {
      const { getAllByText } = renderWithProviders(
        <SimpleParagraph>**First** normal **Second**</SimpleParagraph>,
      );
      const boldElements = getAllByText(/First|Second/);
      expect(boldElements).toHaveLength(2);
      boldElements.forEach((element) => {
        expect(element).toHaveStyle({ fontWeight: "bold" });
      });
    });

    it("should handle multiline text", () => {
      const { getByText } = renderWithProviders(
        <SimpleParagraph>{"Line 1\nLine 2"}</SimpleParagraph>,
      );
      expect(getByText("Line 1")).toBeDefined();
      expect(getByText("Line 2")).toBeDefined();
    });
  });

  describe("Code Block Rendering", () => {
    it("should render code blocks correctly", () => {
      const { getByTestId } = renderWithProviders(
        <SimpleParagraph>
          {"Some text\n```javascript\nconst x = 1;\n```"}
        </SimpleParagraph>,
      );
      const codeBlock = getByTestId("code-block");
      expect(codeBlock).toBeDefined();
      expect(codeBlock.textContent).toBe("const x = 1;");
    });

    it("should preserve code block language", () => {
      const { getByTestId } = renderWithProviders(
        <SimpleParagraph>
          {"```typescript\nconst x: number = 1;\n```"}
        </SimpleParagraph>,
      );
      expect(getByTestId("code-block")).toBeDefined();
    });
  });

  describe("Mixed Content", () => {
    it("should handle mixed text and code blocks", () => {
      const { getByText, getByTestId } = renderWithProviders(
        <SimpleParagraph>
          {"Normal text\n```js\ncode\n```\n**bold** text"}
        </SimpleParagraph>,
      );
      expect(getByText("Normal text")).toBeDefined();
      expect(getByTestId("code-block")).toBeDefined();
      expect(getByText("bold")).toHaveStyle({ fontWeight: "bold" });
    });
  });

  describe("Edge Cases", () => {
    it("should handle empty string", () => {
      const { container } = renderWithProviders(
        <SimpleParagraph>{""}</SimpleParagraph>,
      );
      expect(container.firstChild).toBeDefined();
      expect(container.textContent).toBe("");
    });
  });
});
