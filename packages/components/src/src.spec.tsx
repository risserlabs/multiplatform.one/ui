/**
 * File: /src/src.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 01-01-1970 00:00:00
 * Author: Tarun2811
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { fireEvent, render, screen } from "@testing-library/react";
import { cleanup } from "@testing-library/react";
import { Button, Text, YStack } from "tamagui";
import { afterEach } from "vitest";
import { describe, expect, it, vi } from "vitest";
import { useAutoText } from "./helpers";
import * as components from "./index";
import { TintFamiliesProvider, useTint } from "./tints";
import { ThemeTint, ThemeTintAlt } from "./tints";

vi.mock("one", () => ({
  createApp: vi.fn(),
  usePathname: vi.fn(),
}));
afterEach(() => {
  cleanup();
});

const TestComponent = () => {
  const { setFamily, setTintIndex, tintIndex, name } = useTint();
  return (
    <YStack>
      <Button onPress={() => setFamily("halloween")}>
        Set Family to Halloween
      </Button>
      <Button onPress={() => setTintIndex(tintIndex + 1)}>
        Increment Tint Index
      </Button>
      <Text data-testid="tint-index">{tintIndex}</Text>
      <Text data-testid="tint-name">{name}</Text>
    </YStack>
  );
};

describe("TintFamiliesProvider", () => {
  it("should provide default tint family and index", () => {
    const TestComponent = () => {
      const { name, tintIndex } = useTint();
      return (
        <YStack>
          <Text>{name}</Text>
          <Text>{tintIndex}</Text>
        </YStack>
      );
    };
    const { getByText } = render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    expect(getByText("tamagui").textContent).toBe("tamagui");
    expect(getByText("-3").textContent).toBe("-3");
  });

  it("should change tint family", async () => {
    const TestComponent = () => {
      const { name, setFamily } = useTint();
      return (
        <YStack>
          <Text>{name}</Text>
          <Button onPress={() => setFamily("halloween")}>Change Family</Button>
        </YStack>
      );
    };
    const { getByText, findByText } = render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    expect(getByText("tamagui").textContent).toBe("tamagui");
    getByText("Change Family").click();
    expect(await findByText("halloween")).toBeInTheDocument();
  });

  it("should set tint index correctly", async () => {
    const TestComponent = () => {
      const { tintIndex, setTintIndex } = useTint();
      return (
        <YStack>
          <Text>{tintIndex}</Text>
          <Button onPress={() => setTintIndex(2)}>Set Tint Index</Button>
        </YStack>
      );
    };
    const { getByText, findByText } = render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    expect(getByText("-3").textContent).toBe("-3");
    getByText("Set Tint Index").click();
    expect(await findByText("2")).toBeInTheDocument();
  });

  it("should set family correctly", () => {
    render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    fireEvent.click(screen.getByText("Set Family to Halloween"));
    expect(screen.getByText("halloween")).toBeInTheDocument();
  });

  it("should set family to halloween", () => {
    render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    fireEvent.click(screen.getByText("Set Family to Halloween"));
  });

  it("should call setTintIndex correctly", () => {
    const mockSetTintIndex = vi.fn();
    const TestComponent = () => {
      const { setTintIndex } = useTint();
      return (
        <Button
          onPress={() => {
            setTintIndex(2);
            mockSetTintIndex();
          }}
        >
          Set Tint Index
        </Button>
      );
    };
    render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    fireEvent.click(screen.getByText("Set Tint Index"));
    expect(mockSetTintIndex).toHaveBeenCalled();
  });

  it("should increment tint index correctly", () => {
    render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    fireEvent.click(screen.getByText("Increment Tint Index"));
    expect(screen.getByTestId("tint-index")).toHaveTextContent("-2");
  });

  it("should render children when not disabled", () => {
    const { getByText } = render(
      <TintFamiliesProvider>
        <ThemeTint>
          <Text>Visible Text</Text>
        </ThemeTint>
      </TintFamiliesProvider>,
    );
    expect(getByText("Visible Text")).toBeInTheDocument();
  });

  it("should initialize with the correct tint index", () => {
    const TestComponent = () => {
      const { tintIndex } = useTint();
      return <Text>{tintIndex}</Text>;
    };
    const { getByText } = render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    expect(getByText("-3").textContent).toBe("-3");
  });

  it("should increment the tint index correctly", async () => {
    const TestComponent = () => {
      const { tintIndex, setNextTint } = useTint();
      return (
        <YStack>
          <Text>{tintIndex}</Text>
          <Button onPress={setNextTint}>Next Tint</Button>
        </YStack>
      );
    };
    const { getByText, findByText } = render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    expect(getByText("-3").textContent).toBe("-3");
    getByText("Next Tint").click();
    expect(await findByText("-2")).toBeInTheDocument();
  });

  it("should set the next tint family correctly", async () => {
    const TestComponent = () => {
      const { name, setNextTintFamily } = useTint();
      return (
        <YStack>
          <Text>{name}</Text>
          <Button onPress={setNextTintFamily}>Next Family</Button>
        </YStack>
      );
    };
    const { getByText, findByText } = render(
      <TintFamiliesProvider>
        <TestComponent />
      </TintFamiliesProvider>,
    );
    expect(getByText("tamagui").textContent).toBe("tamagui");
    getByText("Next Family").click();
    expect(await findByText("valentine")).toBeInTheDocument();
  });

  it("should render ThemeTintAlt children when not disabled", () => {
    const { getByText } = render(
      <TintFamiliesProvider>
        <ThemeTintAlt>
          <Text>Visible Text</Text>
        </ThemeTintAlt>
      </TintFamiliesProvider>,
    );
    expect(getByText("Visible Text")).toBeInTheDocument();
  });
});

describe("Index File", () => {
  it("should export all components correctly", () => {
    expect(components).toHaveProperty("Form");
    expect(components).toHaveProperty("useFormContext");
  });
});

describe("useAutoText", () => {
  it("should render Text component when children is a string", () => {
    const { getByText } = render(useAutoText("Hello World", Text));
    expect(getByText("Hello World")).toBeInTheDocument();
  });

  it("should return children as is when not a string", () => {
    const { container } = render(useAutoText(<Text>Not a string</Text>));
    expect(container.querySelector("span")).toBeInTheDocument();
  });
});

describe("Test Setup", () => {
  afterEach(() => {
    cleanup();
  });
  it("should mock matchMedia correctly", () => {
    const mediaQuery = window.matchMedia("(prefers-color-scheme: dark)");
    expect(mediaQuery).toBeDefined();
    expect(mediaQuery.matches).toBe(false);
  });

  it("should call matchMedia with the correct query", () => {
    const mediaQuery = window.matchMedia("(prefers-color-scheme: dark)");
    expect(mediaQuery.media).toBe("(prefers-color-scheme: dark)");
  });

  it("should have a working TextEncoder mock", () => {
    const encoder = new TextEncoder();
    const encoded = encoder.encode("Hello");
    expect(encoded).toBeInstanceOf(Uint8Array);
    expect(encoded.length).toBe(5);
    expect(encoded[0]).toBe(72);
  });

  it("should throw error when TextEncoder input is not a string", () => {
    const encoder = new TextEncoder();
    expect(() => encoder.encode(123 as any)).toThrow(TypeError);
  });

  it("should handle empty string input for TextEncoder", () => {
    const encoder = new TextEncoder();
    const encoded = encoder.encode("");
    expect(encoded).toBeInstanceOf(Uint8Array);
    expect(encoded.length).toBe(0);
  });

  it("should call matchMedia and return correct values for multiple queries", () => {
    const mediaQueryDark = window.matchMedia("(prefers-color-scheme: dark)");
    const mediaQueryLight = window.matchMedia("(prefers-color-scheme: light)");
    expect(mediaQueryDark.matches).toBe(false);
    expect(mediaQueryLight.matches).toBe(false);
  });
});
