/**
 * File: /src/table/table.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 11-02-2025 05:02:36
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { config } from "@tamagui/config";
import { render } from "@testing-library/react";
import { TamaguiProvider, Text, YStack, createTamagui } from "tamagui";
import { describe, expect, it } from "vitest";
import { TableCell } from "./TableCell";
import { TableCol } from "./TableCol";
import { Table } from "./index";

const tamaguiConfig = createTamagui(config);

describe("Table", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  const defaultProps = {
    heading: "Test Table",
    children: (
      <>
        <TableCol>
          <TableCell>Column 1</TableCell>
          <TableCell>Row 1, Cell 1</TableCell>
          <TableCell>Row 2, Cell 1</TableCell>
        </TableCol>
        <TableCol>
          <TableCell>Column 2</TableCell>
          <TableCell>Row 1, Cell 2</TableCell>
          <TableCell>Row 2, Cell 2</TableCell>
        </TableCol>
      </>
    ),
  };

  const renderTable = (props = {}) => {
    return renderWithProviders(<Table {...defaultProps} {...props} />);
  };

  describe("Basic Rendering", () => {
    it("should render successfully with default props", () => {
      const { container } = renderTable();
      const tableFrame = container.querySelector("[data-tamagui-name='Table']");
      expect(tableFrame).toBeDefined();
    });

    it("should render with custom heading", () => {
      const customHeading = "Custom Table Header";
      const { getByText } = renderTable({ heading: customHeading });
      expect(getByText(customHeading)).toBeDefined();
    });

    it("should render without heading", () => {
      const { container } = renderTable({ heading: undefined });
      const tableFrame = container.querySelector("[data-tamagui-name='Table']");
      expect(tableFrame).toBeDefined();
    });
  });

  describe("Table Structure", () => {
    it("should render columns correctly", () => {
      const { getByText } = renderTable({
        children: (
          <>
            <TableCol>
              <TableCell>Test Column 1</TableCell>
            </TableCol>
            <TableCol>
              <TableCell>Test Column 2</TableCell>
            </TableCol>
          </>
        ),
      });
      expect(getByText("Test Column 1")).toBeDefined();
      expect(getByText("Test Column 2")).toBeDefined();
    });

    it("should render multiple rows correctly", () => {
      const { getByText } = renderTable();
      expect(getByText("Row 1, Cell 1")).toBeDefined();
      expect(getByText("Row 1, Cell 2")).toBeDefined();
      expect(getByText("Row 2, Cell 1")).toBeDefined();
      expect(getByText("Row 2, Cell 2")).toBeDefined();
    });

    it("should handle empty columns", () => {
      const { container } = renderTable({
        children: <TableCol />,
      });
      const tableFrame = container.querySelector("[data-tamagui-name='Table']");
      expect(tableFrame).toBeDefined();
    });
  });

  describe("Styling and Layout", () => {
    it("should apply custom styles to table frame", () => {
      const { container } = renderTable({
        backgroundColor: "$blue10",
        borderRadius: "$6",
      });
      const tableFrame = container.querySelector("[data-tamagui-name='Table']");
      expect(tableFrame).toBeDefined();
    });

    it("should handle overflow scroll", () => {
      const { container } = renderTable({
        overflow: "scroll",
      });
      const tableFrame = container.querySelector("[data-tamagui-name='Table']");
      expect(tableFrame).toBeDefined();
    });
  });

  describe("Cell Customization", () => {
    it("should render cells with custom styling", () => {
      const { getByText } = renderTable({
        children: (
          <TableCol>
            <TableCell backgroundColor="$blue10" color="$color12">
              Styled Cell
            </TableCell>
          </TableCol>
        ),
      });
      expect(getByText("Styled Cell")).toBeDefined();
    });
  });

  describe("Edge Cases", () => {
    it("should handle large data sets", () => {
      const largeCols = Array.from({ length: 5 }, (_, colIndex) => (
        <TableCol key={colIndex}>
          {Array.from({ length: 5 }, (_, rowIndex) => (
            <TableCell key={rowIndex}>
              Cell {colIndex}-{rowIndex}
            </TableCell>
          ))}
        </TableCol>
      ));

      const { container } = renderTable({
        children: largeCols,
      });
      const tableFrame = container.querySelector("[data-tamagui-name='Table']");
      expect(tableFrame).toBeDefined();
    });

    it("should handle nested content", () => {
      const { getByText } = renderTable({
        children: (
          <TableCol>
            <TableCell>
              <YStack>
                <Text>Nested Content</Text>
              </YStack>
            </TableCell>
          </TableCol>
        ),
      });
      expect(getByText("Nested Content")).toBeDefined();
    });
  });
});
