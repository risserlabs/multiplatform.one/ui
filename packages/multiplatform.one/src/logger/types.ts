/*
 * File: /src/logger/types.ts
 * Project: multiplatform.one
 * File Created: 21-01-2025 14:49:02
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { LogLevelDesc } from "loglevel";
import type { PlatformName } from "../platform/index";

export interface ILogObj {
  [key: string]: unknown;
}

export enum LogLevel {
  Trace = 0,
  Debug = 1,
  Info = 2,
  Warn = 3,
  Error = 4,
  Silent = 5,
}

export enum LogSource {
  Main = "main",
  Renderer = "renderer",
  Default = "default",
}

export interface LogPayload {
  args?: unknown[];
  level: LogLevel;
  message: unknown;
  platform: PlatformName;
  timestamp: string;
}

export interface LoggerMetadata {
  platform?: PlatformName;
}

export interface LoggerOptions {
  metadata?: LoggerMetadata;
  name?: string;
  type?: "pretty" | "json";
  level?: LogLevelDesc;
}
