/**
 * File: /src/types.tsx
 * Project: @multiplatform.one/router
 * File Created: 23-01-2025 15:28:42
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { OneRouter } from "one";
import type { CSSProperties, ReactNode } from "react";

export type Href = OneRouter.Href<{ __branded__: any }>;
export type LoadingState = "loading" | "loaded" | "error";
export type ResultState = "success" | "error";
export type RootStateListener = (state: ResultState) => void;
export type LoadingStateListener = (state: LoadingState) => void;

export type InputRouteParamsBlank = OneRouter.InputRouteParamsBlank;
export type InputRouteParams<T extends string> = OneRouter.InputRouteParams<T>;

export interface Router {
  back: () => void;
  canGoBack: () => boolean;
  push: (href: Href) => void;
  navigate: (href: Href) => void;
  replace: (href: Href) => void;
  dismiss: (count?: number) => void;
  dismissAll: () => void;
  canDismiss: () => boolean;
  setParams: <T extends string = "">(
    params?: T extends "" ? InputRouteParamsBlank : InputRouteParams<T>,
  ) => void;
  subscribe: (listener: RootStateListener) => () => void;
  onLoadState: (listener: LoadingStateListener) => () => void;
}

export type UseRouter = () => Router;
export type UsePathname = () => string;
export type UseParams = () => Record<string, string>;

export interface LinkProps {
  href: Href;
  replace?: boolean;
  children?: ReactNode;
  style?: CSSProperties;
  className?: string;
  [key: string]: any;
}

export interface LinkComponent {
  (props: LinkProps): JSX.Element;
  resolveHref: (href: Href) => string;
}
