/*
 * File: /src/AsyncStorage/index.electron.ts
 * Project: @multiplatform.one/use-store
 * File Created: 10-01-2025 19:59:00
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type TAsyncStorage from "@react-native-async-storage/async-storage";

declare const window: Window & {
  electron: {
    store: {
      get: (key: string) => Promise<any>;
      set: (key: string, value: any) => Promise<void>;
      delete: (key: string) => Promise<void>;
      clear: () => Promise<void>;
      has: (key: string) => Promise<boolean>;
      keys: () => Promise<string[]>;
    };
  };
};

export const AsyncStorage: typeof TAsyncStorage = {
  async getItem(key: string) {
    const value = await window.electron.store.get(key);
    return value ?? null;
  },

  async setItem(key: string, value: string) {
    await window.electron.store.set(key, value);
  },

  async removeItem(key: string) {
    await window.electron.store.delete(key);
  },

  async clear() {
    await window.electron.store.clear();
  },

  async getAllKeys() {
    return await window.electron.store.keys();
  },

  async multiGet(keys: readonly string[]) {
    const results = await Promise.all(
      keys.map(
        async (key) =>
          [key, await this.getItem(key)] as [string, string | null],
      ),
    );
    return results;
  },

  async multiSet(keyValuePairs: readonly [string, string][]) {
    await Promise.all(
      keyValuePairs.map(([key, value]) => this.setItem(key, value)),
    );
  },

  async multiRemove(keys: readonly string[]) {
    await Promise.all(keys.map((key) => this.removeItem(key)));
  },

  async mergeItem(key: string, value: string) {
    const existing = await this.getItem(key);
    if (existing !== null) {
      try {
        const existingObj = JSON.parse(existing);
        const valueObj = JSON.parse(value);
        const merged = JSON.stringify({ ...existingObj, ...valueObj });
        await this.setItem(key, merged);
      } catch (e) {
        await this.setItem(key, value);
      }
    } else {
      await this.setItem(key, value);
    }
  },

  async multiMerge(keyValuePairs: readonly [string, string][]) {
    await Promise.all(
      keyValuePairs.map(([key, value]) => this.mergeItem(key, value)),
    );
  },

  flushGetRequests() {
    return;
  },
};
