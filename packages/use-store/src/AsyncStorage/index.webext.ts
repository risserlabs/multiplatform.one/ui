/*
 * File: /src/AsyncStorage/index.webext.ts
 * Project: @multiplatform.one/use-store
 * File Created: 10-01-2025 19:59:00
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type TAsyncStorage from "@react-native-async-storage/async-storage";
import browser from "webextension-polyfill";

export const AsyncStorage: typeof TAsyncStorage = {
  async getItem(key: string) {
    const result = await browser.storage.local.get(key);
    return result[key] ?? null;
  },

  async setItem(key: string, value: string) {
    await browser.storage.local.set({ [key]: value });
  },

  async removeItem(key: string) {
    await browser.storage.local.remove(key);
  },

  async clear() {
    await browser.storage.local.clear();
  },

  async getAllKeys() {
    const items = await browser.storage.local.get(null);
    return Object.keys(items);
  },

  async multiGet(keys: readonly string[]) {
    const items = await browser.storage.local.get([...keys]);
    return keys.map((key) => [key, items[key] ?? null]);
  },

  async multiSet(keyValuePairs: readonly [string, string][]) {
    const items = Object.fromEntries(keyValuePairs);
    await browser.storage.local.set(items);
  },

  async multiRemove(keys: readonly string[]) {
    await browser.storage.local.remove([...keys]);
  },

  async mergeItem(key: string, value: string) {
    const existing = await this.getItem(key);
    if (existing !== null) {
      try {
        const existingObj = JSON.parse(existing);
        const valueObj = JSON.parse(value);
        const merged = JSON.stringify({ ...existingObj, ...valueObj });
        await this.setItem(key, merged);
      } catch (err) {
        await this.setItem(key, value);
      }
    } else {
      await this.setItem(key, value);
    }
  },

  async multiMerge(keyValuePairs: readonly [string, string][]) {
    await Promise.all(
      keyValuePairs.map(([key, value]) => this.mergeItem(key, value)),
    );
  },

  flushGetRequests() {
    return;
  },
};
