/*
 * File: /src/meshConfig.ts
 * Project: @multiplatform.one/utils
 * File Created: 18-01-2025 18:38:49
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * File: /src/meshConfig.ts
 * Project: utils
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 */

export const meshConfig = {
  sources: [
    ...(process.env.MESH_API === "1"
      ? [
          {
            name: "API",
            handler: {
              graphql: {
                endpoint: `http://localhost:${process.env.API_PORT}/graphql`,
                subscriptionsEndpoint: `ws://localhost:${process.env.API_PORT}/graphql`,
                subscriptionsProtocol: "WS",
                operationHeaders: {
                  Authorization: "{context.headers.Authorization}",
                  Cookie: "{context.headers.Cookie}",
                },
              },
            },
            transforms: [
              {
                prefix: {
                  mode: "wrap",
                  value: "API",
                },
              },
              {
                filterSchema: {
                  mode: "wrap",
                  filters: [],
                },
              },
            ],
          },
        ]
      : []),
    ...(process.env.MESH_FRAPPE === "1"
      ? [
          {
            name: "Frappe",
            handler: {
              graphql: {
                endpoint: `${process.env.FRAPPE_BASE_URL}/api/method/graphql`,
                subscriptionsEndpoint:
                  `${process.env.FRAPPE_BASE_URL}/api/method/graphql`.replace(
                    /^https?/,
                    "ws",
                  ),
                subscriptionsProtocol: "WS",
                operationHeaders: {
                  Authorization: "{context.headers.Authorization}",
                  Cookie: "{context.headers.Cookie}",
                },
              },
            },
            transforms: [
              {
                prefix: {
                  mode: "wrap",
                  value: "Frappe",
                },
              },
              {
                filterSchema: {
                  mode: "wrap",
                  filters: ["Subscription.doc_events"],
                },
              },
            ],
          },
        ]
      : []),
  ],
  additionalEnvelopPlugins: "./envelopPlugins.js",
  serve: {
    browser: false,
  },
  plugins: [
    {
      rateLimit: {
        config: [],
      },
    },
    {
      maxTokens: {
        n: 1000,
      },
    },
    {
      maxDepth: {
        n: 10,
      },
    },
    {
      blockFieldSuggestions: {},
    },
    {
      responseCache: {},
    },
  ],
} as const;
