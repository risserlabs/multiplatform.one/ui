/**
 * File: /components/CreateRoomTable/index.tsx
 * Project: app
 * File Created: 16-01-2025 10:50:08
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { RoomDataType } from "app/screens/create-room";

import { Table, Text, YStack } from "ui";

export const CreateRoomTable = ({ roomData }: { roomData: RoomDataType[] }) => {
  return (
    <YStack gap="$6">
      <Table heading="">
        <Table.Col>
          <Table.Cell backgroundColor="$color3" padding="$4">
            <Text fontSize="$3" color="$color11" fontWeight="bold">
              Room No
            </Text>
          </Table.Cell>
          {roomData.map((_, index) => (
            <Table.Cell key={index} padding="$4">
              <Text fontSize="$4" $sm={{ fontSize: "$3" }}>
                {index + 1}
              </Text>
            </Table.Cell>
          ))}
        </Table.Col>

        <Table.Col>
          <Table.Cell backgroundColor="$color3" padding="$4">
            <Text fontSize="$3" color="$color11" fontWeight="bold">
              Room Name
            </Text>
          </Table.Cell>
          {roomData.map((room, index) => (
            <Table.Cell key={index} padding="$4">
              <Text fontSize="$4" $sm={{ fontSize: "$3" }}>
                {room.roomName}
              </Text>
            </Table.Cell>
          ))}
        </Table.Col>

        <Table.Col>
          <Table.Cell backgroundColor="$color3" padding="$4">
            <Text fontSize="$3" color="$color11" fontWeight="bold">
              Room Code
            </Text>
          </Table.Cell>
          {roomData.map((room, index) => (
            <Table.Cell key={index} padding="$4">
              <Text fontSize="$4" $sm={{ fontSize: "$3" }}>
                {room.roomCode}
              </Text>
            </Table.Cell>
          ))}
        </Table.Col>

        <Table.Col>
          <Table.Cell backgroundColor="$color3" padding="$4">
            <Text fontSize="$3" color="$color11" fontWeight="bold">
              Invite Link
            </Text>
          </Table.Cell>
          {roomData.map((room, index) => (
            <Table.Cell key={index} padding="$4">
              <Text fontSize="$4" $sm={{ fontSize: "$3" }}>
                {room.inviteLink}
              </Text>
            </Table.Cell>
          ))}
        </Table.Col>
      </Table>
    </YStack>
  );
};
