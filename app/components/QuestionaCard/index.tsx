/**
 * Project: app
 * File Created: 18-01-2025 10:27:58
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Clock } from "@tamagui/lucide-icons";
import { useRouter } from "one";
import { useEffect, useState } from "react";
import {
  Button,
  Card,
  H1,
  H2,
  Image,
  Label,
  Progress,
  RadioGroup,
  Text,
  XStack,
  YStack,
} from "ui";
import checkIcon from "../../assets/correct.svg";
import { ResultCard } from "../ResultsCard";

interface Question {
  id: number;
  text: string;
  options: string[];
  correctAnswer: number;
}

export const QuestionsCard = () => {
  const [timeLeft, setTimeLeft] = useState(60);
  const [showWarning, setShowWarning] = useState(false);
  const [isTimeUp, setIsTimeUp] = useState(false);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState<string>("none");
  const [isQuizComplete, setIsQuizComplete] = useState(false);
  const [showScore, setShowScore] = useState(false);
  const [userAnswers, setUserAnswers] = useState<string[]>([]);

  const questions: Question[] = [
    {
      id: 1,
      text: "Get my 1st Million",
      options: ["John Doe", "William", " Smith", "Logan"],
      correctAnswer: 0,
    },
    {
      id: 2,
      text: "Go to Goa",
      options: ["John Doe", "William", " Smith", "Logan"],
      correctAnswer: 1,
    },
  ];
  const router = useRouter();
  const resetTimer = () => {
    setTimeLeft(60);
    setShowWarning(false);
    setIsTimeUp(false);
  };

  useEffect(() => {
    const timer = setInterval(() => {
      setTimeLeft((prevTime) => {
        if (prevTime <= 0) {
          clearInterval(timer);
          setIsTimeUp(true);
          return 0;
        }
        return prevTime - 1;
      });
    }, 1000);

    return () => clearInterval(timer);
  }, [currentQuestionIndex]);

  useEffect(() => {
    if (timeLeft <= 20) {
      setShowWarning(true);
    }
  }, [timeLeft]);

  useEffect(() => {
    if (showScore || isTimeUp) {
      const timer = setTimeout(() => {
        if (currentQuestionIndex < questions.length - 1) {
          setCurrentQuestionIndex((prev) => prev + 1);
          setTimeLeft(60);
          setIsTimeUp(false);
          setShowScore(false);
          setShowWarning(false);
          setSelectedAnswer("none");
        } else {
          setIsQuizComplete(true);
          router.push("/game-over/123");
        }
      }, 2000);

      return () => clearTimeout(timer);
    }
  }, [showScore, isTimeUp, currentQuestionIndex, questions.length]);

  const progressValue = (timeLeft / 60) * 100;

  const timerColor = timeLeft > 20 ? "green" : "red";
  const currentQuestion = questions[currentQuestionIndex];

  if (isQuizComplete) {
    return <ResultCard />;
  }

  if (showScore || isTimeUp) {
    return (
      <Card
        backgroundColor="$color1"
        minWidth="320"
        borderRadius="$4"
        padding="$4"
        width="30%"
        height={400}
        ai="center"
        jc="center"
        $sm={{ height: 300, width: "50%" }}
      >
        <YStack gap="$4" ai="center">
          <Image
            src={checkIcon}
            width={100}
            height={100}
            $sm={{ width: 80, height: 80 }}
          />
          <H1
            color="$green10"
            fontSize={30}
            $sm={{ fontSize: 20, letterSpacing: 0.4 }}
          >
            Correct{" "}
          </H1>
          <Text fontSize={50} color="$green10" $sm={{ fontSize: 28 }}>
            +1
          </Text>
          {currentQuestionIndex < questions.length - 1 ? (
            <Text
              fontSize={20}
              $sm={{ letterSpacing: 0.2, fontSize: 18 }}
              textAlign="center"
            >
              Moving to next question...
            </Text>
          ) : (
            <Text fontSize={20}>Showing results...</Text>
          )}
        </YStack>
      </Card>
    );
  }

  return (
    <YStack ai="center" jc="center" paddingTop="$4" width="100%" minWidth={320}>
      <Card
        backgroundColor="$color1"
        borderRadius="$4"
        width="100%"
        maxWidth={600}
        minWidth={320}
        padding="$4"
        gap="$4"
        ai="center"
        $sm={{
          padding: "$2",
          gap: "$2",
          borderRadius: "$2",
        }}
      >
        <YStack space="$4" width="100%" $sm={{ space: "$2" }}>
          <XStack
            width="100%"
            ai="center"
            jc="space-between"
            $sm={{ scale: 0.9 }}
          >
            <Text fontSize="$5" $sm={{ fontSize: "$" }}>
              Question:{currentQuestion.id}
            </Text>
            <XStack ai="center" space="$2" marginLeft="$2">
              <Clock size="$1" $sm={{ size: "$0.75" }} color="white" />
              <Text color={timerColor} fontWeight="bold">
                {timeLeft}s
              </Text>
            </XStack>
          </XStack>

          <Progress value={progressValue} backgroundColor="$backgroundFocus">
            <Progress.Indicator
              animation="bouncy"
              backgroundColor={timerColor}
            />
          </Progress>
          <YStack height={50} jc="center" $sm={{ height: 40, scale: 0.9 }}>
            {showWarning && !isTimeUp && (
              <Text
                color="$red10"
                textAlign="center"
                animation="bouncy"
                borderWidth={1}
                borderColor="$red8"
                borderRadius="$2"
                padding="$2"
                letterSpacing={0.5}
                $sm={{
                  fontSize: "$3",
                  padding: "$1",
                }}
              >
                Hurry up! Less than {timeLeft} seconds remaining!
              </Text>
            )}

            {isTimeUp && (
              <Text
                color="white"
                textAlign="center"
                borderWidth={1}
                borderColor="$red8"
                borderRadius="$2"
                padding="$2"
                letterSpacing={0.5}
                $sm={{
                  fontSize: "$3",
                  padding: "$1",
                }}
              >
                Time's up! You can't select any options now.
              </Text>
            )}
          </YStack>
          <H2
            letterSpacing={0.5}
            fontSize="$9"
            textAlign="center"
            $sm={{
              fontSize: "$4",
              paddingHorizontal: "$2",
            }}
          >
            {currentQuestion.text}
          </H2>
          <RadioGroup
            value={selectedAnswer}
            onValueChange={setSelectedAnswer}
            name="options"
            disabled={isTimeUp}
          >
            <YStack space="$3" $sm={{ space: "$2" }}>
              {currentQuestion.options.map((option, index) => (
                <XStack key={index} space="$2" ai="center" width="100%">
                  <Label
                    htmlFor={`opt${index + 1}`}
                    flex={1}
                    backgroundColor="$transparent"
                    borderWidth={1}
                    borderColor="$borderColor"
                    borderRadius="$4"
                    padding="$3"
                    pressStyle={{
                      backgroundColor: "$backgroundFocus",
                    }}
                  >
                    <XStack space="$2" ai="center">
                      <RadioGroup.Item
                        value={String(index + 1)}
                        id={`opt${index + 1}`}
                        size="$4"
                        backgroundColor="$background"
                        borderColor="$borderColor"
                      >
                        <RadioGroup.Indicator
                          width="60%"
                          height="60%"
                          backgroundColor="white"
                        />
                      </RadioGroup.Item>
                      <Text
                        letterSpacing={0.3}
                        fontSize="$4"
                        $sm={{
                          fontSize: "$3",
                        }}
                      >
                        {option}
                      </Text>
                    </XStack>
                  </Label>
                </XStack>
              ))}
            </YStack>
          </RadioGroup>
          <XStack
            space="$4"
            jc="flex-end"
            marginTop="$4"
            $sm={{
              space: "$2",
              marginTop: "$2",
            }}
          >
            <Button
              width="$12"
              onPress={() => setShowScore(true)}
              $sm={{
                width: "48%",
                scale: 0.9,
              }}
            >
              <Text>Submit</Text>
            </Button>
          </XStack>
        </YStack>
      </Card>
    </YStack>
  );
};
