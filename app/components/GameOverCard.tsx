import { Home, Play } from "@tamagui/lucide-icons";
import { useRouter } from "one";
/**
 * File: /components/GameOverCard.tsx
 * Project: app
 * File Created: 06-01-2025 14:46:31
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Button, Card, Text, XStack, YStack } from "ui";
import ResultCard from "./ResultsCard";
export const GameOverCard = () => {
  const router = useRouter();
  return (
    <YStack
      ai="center"
      jc="center"
      paddingTop="$10"
      w="90%"
      maxWidth={700}
      minWidth={300}
    >
      <Card
        backgroundColor="$color1"
        borderRadius="$3"
        width="100%"
        maxWidth={500}
        minWidth={300}
        padding="$4"
        gap="$4"
        alignItems="center"
      >
        <Card.Header gap="$2">
          <Text fontSize="$10" fontWeight="bold" $sm={{ fontSize: "$8" }}>
            Game Over
          </Text>
          <Text
            alignSelf="center"
            fontSize="$5"
            opacity={0.5}
            $sm={{ fontSize: "$4" }}
          >
            Thank you for playing!
          </Text>
        </Card.Header>
        <ResultCard />

        <XStack flex={1} gap="$4" $sm={{ gap: "$2" }}>
          <Button
            width="50%"
            fontWeight="bold"
            borderColor="$color12"
            borderRadius="$2"
            backgroundColor="$color1"
            hoverStyle={{
              backgroundColor: "$color10",
              scale: 1.05,
            }}
            padding="$4"
            $sm={{
              scale: 0.9,
              minWidth: "40%",
            }}
            onPress={() => router.push("/join-room")}
          >
            <Play size="$1" />
            Replay quiz
          </Button>

          <Button
            fontWeight="bold"
            width="50%"
            borderRadius="$2"
            backgroundColor="$color12"
            color="$color1"
            borderColor="$color1"
            onPress={() => router.push("/")}
            hoverStyle={{
              backgroundColor: "$color10",
              scale: 1.05,
            }}
            padding="$4"
            $sm={{
              scale: 0.9,
              minWidth: "40%",
            }}
          >
            <Home color="$color1" size="$1" />
            Return Home
          </Button>
        </XStack>
      </Card>
    </YStack>
  );
};
