/**
 * File: /screens/game-over/[id].tsx
 * Project: app
 * File Created: 16-01-2025 15:42:39
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { withAuthenticated } from "@multiplatform.one/keycloak";
import { GameOverCard } from "app/components/GameOverCard/index";
import { YStack } from "ui";
const GameOverScreen = () => {
  return (
    <YStack fullscreen ai="center" jc="center">
      <GameOverCard />
    </YStack>
  );
};

export const Screen = withAuthenticated(GameOverScreen);
