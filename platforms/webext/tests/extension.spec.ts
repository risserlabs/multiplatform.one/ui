/**
 * File: /tests/extension.spec.ts
 * Project: React WebExt
 * File Created: 16-01-2025 17:27:27
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from "node:fs";
import path from "node:path";
import { chromium } from "@playwright/test";
import { expect, test } from "@playwright/test";
import { isHeadless } from "../env";

test("verify extension", async () => {
  const extensionPath = path.join(__dirname, "../extension");
  const requiredFiles = [
    "manifest.json",
    "dist/background/index.mjs",
    ...Object.values(
      await fs.promises.readdir(path.join(__dirname, "../src/views"), {
        withFileTypes: true,
      }),
    ).map((file) => `dist/src/views/${file.name}/index.html`),
  ];
  expect(fs.existsSync(extensionPath)).toBeTruthy();
  const missingFiles = requiredFiles.filter(
    (file) => !fs.existsSync(path.join(extensionPath, file)),
  );
  expect(missingFiles).toHaveLength(0);
});

const userDataDir = path.join(__dirname, "../test-user-data");
const extensionPath = path.join(__dirname, "../extension");

test("extension functionality", async () => {
  test.skip(isHeadless, "skipping in headless mode");
  const context = await chromium.launchPersistentContext(userDataDir, {
    headless: false,
    args: [
      `--disable-extensions-except=${extensionPath}`,
      `--load-extension=${extensionPath}`,
      "--no-sandbox",
    ],
  });
  try {
    const worker = context.serviceWorkers()[0];
    if (!worker) throw new Error("service worker not found");
    const extensionId = worker.url().split("/")[2];
    const popupUrl = `chrome-extension://${extensionId}/dist/src/views/popup/index.html`;
    const popup = await context.newPage();
    await popup.goto(popupUrl, { waitUntil: "domcontentloaded" });
    expect(await popup.evaluate(() => window.location.href)).toBe(popupUrl);
    expect(await popup.title()).toBe("Popup");
    const popupTitle = popup.getByRole("heading", { name: "React WebExt" });
    const popupText = popup.getByText("This is the popup page");
    const openSidepanelButton = popup.getByRole("button", {
      name: "Open Sidepanel",
    });
    const openOptionsButton = popup.getByRole("button", {
      name: "Open Options",
    });
    await expect(popupTitle).toBeVisible({ timeout: 5000 });
    await expect(popupTitle).toBeEnabled();
    await expect(popupText).toBeVisible({ timeout: 5000 });
    await expect(openSidepanelButton).toBeVisible({ timeout: 5000 });
    await expect(openSidepanelButton).toBeEnabled();
    await expect(openOptionsButton).toBeVisible({ timeout: 5000 });
    await expect(openOptionsButton).toBeEnabled();
    const optionsPagePromise = context.waitForEvent("page");
    await openOptionsButton.click();
    const optionsPage = await optionsPagePromise;
    await optionsPage.waitForLoadState("domcontentloaded");
    expect(await optionsPage.title()).toBe("Options");
    const optionsUrl = `chrome-extension://${extensionId}/dist/src/views/options/index.html`;
    expect(await optionsPage.evaluate(() => window.location.href)).toBe(
      optionsUrl,
    );
    const optionsTitle = optionsPage.getByRole("heading", { name: "Options" });
    await expect(optionsTitle).toBeVisible({ timeout: 5000 });
    await expect(optionsTitle).toBeEnabled();
    await popup.bringToFront();
    const tabsBeforeSidepanel = context.pages();
    await openSidepanelButton.click();
    expect(popup.isClosed()).toBeTruthy();
    expect(context.pages()).toHaveLength(tabsBeforeSidepanel.length - 1);
    await optionsPage.close();
  } finally {
    await context.close();
  }
});
