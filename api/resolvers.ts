/*
 * File: /resolvers.ts
 * Project: @service/api
 * File Created: 24-01-2025 08:51:51
 * Author: Vandana Madhireddy
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * File: /resolvers.ts
 * Project: @service/api
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Authorized } from "@multiplatform.one/keycloak-typegraphql";
import type { NonEmptyArray } from "type-graphql";
import { AuthResolver } from "./auth";
import { CountResolver } from "./count";
import {
  AnswerCrudResolver,
  GameCrudResolver,
  // PostCrudResolver,

  GameRoomCrudResolver,
  GuessCrudResolver,
  QuestionCrudResolver,
  UserCrudResolver,
  applyArgsTypesEnhanceMap,
  applyInputTypesEnhanceMap,
  applyModelsEnhanceMap,
  applyOutputTypesEnhanceMap,
  // applyRelationResolversEnhanceMap,
  applyResolversEnhanceMap,
} from "./generated/type-graphql";

export const resolvers: NonEmptyArray<Function> | NonEmptyArray<string> = [
  AuthResolver,
  CountResolver,
  GameRoomCrudResolver,
  UserCrudResolver,
  QuestionCrudResolver,
  AnswerCrudResolver,
  GuessCrudResolver,
  GameCrudResolver,
];

applyResolversEnhanceMap({
  User: {
    _all: [Authorized()],
  },
});
applyArgsTypesEnhanceMap({});
applyInputTypesEnhanceMap({});
applyModelsEnhanceMap({});
applyOutputTypesEnhanceMap({});
// applyRelationResolversEnhanceMap({});
